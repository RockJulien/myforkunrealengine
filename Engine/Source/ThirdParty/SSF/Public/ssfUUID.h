#ifndef _ssfUUID_h_
#define _ssfUUID_h_

#include "ssfBaseDataType.h"

namespace ssf
	{
	extern void ValidateStringUUID( const string &uuid_string );
	}

#endif//_ssfUUID_h_
