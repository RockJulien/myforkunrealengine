#ifndef _ssfAttributeTypes_h_
#define _ssfAttributeTypes_h_

#include "ssfBool.h"
#include "ssfInt8.h"
#include "ssfUInt8.h"
#include "ssfInt16.h"
#include "ssfUint16.h"
#include "ssfInt32.h"
#include "ssfUInt32.h"
#include "ssfInt64.h"
#include "ssfUInt64.h"
#include "ssfFloat.h"
#include "ssfDouble.h"
#include "ssfString.h"

#include "ssfList.h"
#include "ssfIndexedList.h"
#include "ssfTupleList.h"
#include "ssfNamedList.h"
#include "ssfNamedIndexedList.h"
#include "ssfNamedTupleList.h"
#include "ssfNamedIdList.h"

#include "ssfMatrix3x3.h"
#include "ssfMatrix4x4.h"

#include "ssfVector2.h"
#include "ssfVector3.h"
#include "ssfVector4.h"

#include "ssfIndex2.h"
#include "ssfIndex3.h"
#include "ssfIndex4.h"

#endif//_ssfAttributeTypes_h_ 